<?php
  //Connexion a la base de données
  $db = new PDO('mysql:host=localhost;dbname=dufresne_blog;charset=utf8mb4', 'root', 'root');
  if(isset($_GET['tag'])){
    $query = $db->prepare(" SELECT article.id, article.titre, article.date, article.body, article.imgURL, article.coordonnees, article.auteur_id, auteur.id, auteur.nom, article_tag.tag_id
                            FROM article
                            INNER JOIN auteur ON auteur.id=article.auteur_id
                            INNER JOIN article_tag ON article_tag.article_id = article.id
                            WHERE article_tag.tag_id=".$_GET['tag']);
  } else {
    $query = $db->prepare(" SELECT article.id, article.titre, article.date, article.body, article.imgURL, article.coordonnees, article.auteur_id, auteur.id, auteur.nom
                            FROM article
                            INNER JOIN auteur ON auteur.id=article.auteur_id
                            WHERE article.id=".$_GET['id']);
  }
  $query->execute();
  $articles = $query->fetchAll();
  //print_r($articles);

  $query = $db->prepare(" SELECT *
                            FROM tag
                            ORDER BY name");
  $query->execute();
  $tags = $query->fetchAll();

  $query = $db->prepare(" SELECT image.imggallery, image.legende
                          FROM image
                          INNER JOIN article ON article.id=image.article_id
                          WHERE article.id=".$_GET['id']);
$query->execute();
$images = $query->fetchAll();

$query = $db->prepare(" SELECT article.id articleid, article.titre, article.date, article.body, article.imgURL, article.auteur_id, auteur.id auteurid, auteur.nom
                        FROM article
                        INNER JOIN auteur ON auteur.id=article.auteur_id
                        LIMIT 0,3");
$query->execute();
$links = $query->fetchAll();
?>
<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dufresne Loïc | Article</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="foundation-icons/foundation-icons.css">
  </head>
  <body>
        <div class="title-bar title-bar-nav" data-responsive-toggle="main-menu" data-hide-for="medium">
          <a href="index.php"><div class="title-bar-title"><img src="contenu/img/california.png" class="imagenavsmall"></div></a>
          <button class="menu-icon" type="button" data-toggle></button>
          <div class="title-bar-title">Menu</div>
        </div>
        <div class="top-bar top-bar-nav" id="main-menu">
          <div class="top-bar-left">
            <ul class="dropdown dropdownnav menu menunav" data-dropdown-menu>
              <a href="index.php"><li class="menu-text menu-text-nav"><img src="contenu/img/california.png" class="imagenav"></li></a>
            </ul>
          </div>
          <div class="menu-centered">
            <ul class="menu menunav" data-responsive-menu="drilldown medium-dropdown">
              <li class="has-submenu">
                <a href="cv.php">Curriculum Vitae</a>
                <ul class="submenu menu menunav vertical" data-submenu>
                  <li><a href="annexe.php">Annexes</a></li>
                </ul>
              </li>
              <li><a href="blog.php">Blog</a></li>
              <li><a href="euro.php">Euro 2016</a></li>
              <li><a href="formulaire.php">Formulaire</a></li>
            </ul>
          </div>
        </div>

        <div class="orbit baniere" role="region" aria-label="Favorite Space Pictures" data-orbit>
          <ul class="orbit-container">
            <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous Slide</span>&#9664;</button>
            <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next Slide</span>&#9654;</button>
            <li class="orbit-slide is-active">
              <img src="contenu/img/banner1.png">
            </li>
            <li class="orbit-slide">
              <img src="contenu/img/banner2.png">
            </li>
            <li class="orbit-slide">
              <img src="contenu/img/banner3.png">
            </li>
          </ul>
        </div>
        <!-- Header -->
        <header class="row site-header">
        </header>
        <!-- Section -->
        <section class="row site-content">
          <div class="columns small-12 medium-12 large-12 blog_searchbar">
            <div class="top-bar blog_searchbarRadius" id="example-menu">
              <div class="top-bar-left">
                <ul class="dropdown menu" data-dropdown-menu>
                  <li class="menu-text">Mes articles</li>
                  <li><a href="blog.php">Plus récents</a></li>
                  <li><a href="blog.php?dateasc">Plus anciens</a></li>
                  <li><a href="blog.php">Tous les articles</a></li>
                </ul>
              </div>
              <div class="top-bar-right">
                <ul class="menu">
                  <li><input type="search" class="blog_searchinput" placeholder="Search"></li>
                  <li><button type="button" class="button blog_searchbutton">Search</button></li>
                </ul>
              </div>
            </div>
            <span>
              <?php foreach ($tags as $tag):?>
                <a href="blog.php?tag=<?php echo $tag['id'];?>" class="label blog_tags"><?php echo $tag['name'];?></a>
              <?php endForeach; ?>
            </span>
          </div>
          <div class="columns small-12 medium-12 large-12 article_contents">
            <?php foreach ($articles as $article):?>
            <div class="columns small-3 medium-3 large-3 article_contentsLeft">
              <div class="columns small-12 medium-12 large-12 article_imageArticle">
                <img src="<?php echo $article['imgURL'];?>" class="article_imageArticle">
              </div>
              <div class="columns small-12 medium-12 large-12 article_detailArticle">
                <p>Auteur: <?php echo $article['nom'] ;?></p>
                <p>Date: <?php echo $article['date'] ;?></p>
                <iframe src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Flocalhost%3A8888%2FProjetDufresneWebsite%2Farticle.php%3Fid%3D3&layout=button_count&mobile_iframe=true&width=101&height=20&appId" width="101" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
              </div>
            </div>
            <div class="columns small-9 medium-9 large-9 article_texteArticle">
              <h1><?php echo $article['titre'];?></h1>
              <p><?php echo $article['body'];?></p>
            </div>
            <div class="columns small-12 medium-12 large-12 article_gallery">
              <?php foreach ($images as $image):?>
              <div class="galery_responsive">
                <div class="gallery_img">
                  <img src="<?php echo $image['imggallery'];?>" class="gallery_contents">
                  <div class="gallery_texte"><?php echo $image['legende'];?></div>
                </div>
              </div>
              <?php endForeach; ?>
              <div class="clearfix"></div>

              <!-- The Modal -->
              <div id="myModal" class="modal">
                <span class="close">Fermer</span>
                <img class="modal-content" id="img01">
                <div id="caption"></div>
              </div>
            </div>
            <div class="columns small-12 medium-12 large-12 article_map">
              <iframe src="<?php echo $article['coordonnees'];?>" class="article_mapContent"></iframe>
            </div>
            <?php endForeach; ?>
          </div>
          <?php foreach ($links as $link):?>
            <a href="article.php?id=<?php echo $link['articleid']?>">
            <div class="columns small-12 medium-6 large-4 article_links">
              <div class="columns small-12 medium-12 large-12 article_linksContents">
                <p class="article_linksTitre"><?php echo $link['titre'];?></p>
                <div class="columns small-4 medium-4 large-4">
                  <img src="<?php echo $link['imgURL'];?>" class="article_linksImage">
                </div>
                <div class="columns small-8 medium-8 large-8 article_linksText">
                  <?php echo substr ($link['body'],0,60)?>...
                  <p class="blog_suiteArticle">Lire la suite...</p>
                </div>
                <div class="columns small-12 medium-12 large-12 article_linksAuteur">
                  <?php echo $link['date'] ;?>
                  <?php echo $link['nom'] ;?>
                </div>
              </div>
            </div>
          </a>
        <?php endForeach; ?>
        <div class="columns small-12 medium-12 large-12 article_linksArticle">
          <span>
            <?php foreach ($tags as $tag):?>
              <a href="blog.php?tag=<?php echo $tag['id'];?>" class="article_linksTag footer-links"><?php echo $tag['name'];?></a>
            <?php endForeach; ?>
          </span>
        </div>
        </section>
        <!-- Footer -->
        <footer class="site-footer">
          <div class="row">
            <div class="small-12 medium-12 large-4 columns footertop footertopright">
              <p class="footer-links">
                <a href="cv.php">Curriculum Vitae</a>
                <a href="blog.php">Blog</a>
                <a href="euro.php">Euro 2016</a>
                <a href="formulaire.php">Formulaire</a>
              </p>
              <p class="footer-copywrite">Copywrite Loïc Dufresne © 2016</p>
            </div>
            <div class="small-12 medium-6 large-4 columns footertop footertopcenter">
              <ul class="contact">
                <li><p><i class="fi-male"></i>Loïc Dufresne</p></li>
                <li><p><i class="fi-marker"></i>1564 Domdidier</p></li>
                <li><p><i class="fi-telephone"></i>079 519 45 27</p></li>
                <li><p><a href="formulaire.php"><i class="fi-mail"></i>loic.dufresne@bluewin.ch</a></p></li>
              </ul>
            </div>
            <div class="small-12 medium-6 large-4 columns footertop footertopleft">
              <p class="footer-text">Follow me on :</p><br>
              <a href="https://www.facebook.com/dufresne.loic" target="_blank" class="first-links fi-social-facebook"></a>
              <a href="https://twitter.com/3DtoCOMPTON" target="_blank" class="first-links fi-social-twitter"></a>
              <a href="https://plus.google.com/100604848590200407608/posts" target="_blank" class="first-links fi-social-google-plus"></a><br><br>
              <a href="https://www.instagram.com/iam_dudu/" target="_blank" class="first-links fi-social-instagram"></a>
              <a href="https://www.youtube.com/channel/UC--YfpWVZ2Y5ais0XjZkQlg" target="_blank" class="first-links fi-social-youtube"></a>
              <a href="https://www.spotify.com/ch-fr/" target="_blank" class="first-links fi-social-spotify"></a>
            </div>
              <a href="blog.php">
                <div class="small-12 medium-12 large-12 columns footercenterleft">
                  <p class="second-links">Jetez un coup d'oeil aux derniers articles !</p>
                </div>
              </a>
              <a href="annexe.php">
                <div class="small-12 medium-12 large-12 columns footercentercenter">
                  <p class="second-links test">Découvrez mes projets ici !</p>
                </div>
              </a>
              <a href="formulaire.php">
                <div class="small-12 medium-12 large-12 columns footercenterright">
                  <p class="second-links">Contactez moi !</p>
                </div>
              </a>
          </div>
        </footer>
        <div id="map">
        </div>
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJnR5M5g9XBX64Bn0aKeYer0O2GIBEdcY&callback=initMap">
    </script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>

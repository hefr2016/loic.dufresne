<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dufresne Loïc | Template</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="foundation-icons/foundation-icons.css">
  </head>
  <link rel="stylesheet" href="css/foundation.css">
  <link rel="stylesheet" href="css/app.css">
  <link rel="stylesheet" href="css/header.css">
  <link rel="stylesheet" href="css/footer.css">
  <link rel="stylesheet" href="css/menu.css">
  <link rel="stylesheet" href="foundation-icons/foundation-icons.css">
</head>
<body>
      <div class="title-bar title-bar-nav" data-responsive-toggle="main-menu" data-hide-for="medium">
        <a href="index.php"><div class="title-bar-title"><img src="contenu/img/california.png" class="imagenavsmall"></div></a>
        <button class="menu-icon" type="button" data-toggle></button>
        <div class="title-bar-title">Menu</div>
      </div>
      <div class="top-bar top-bar-nav" id="main-menu">
        <div class="top-bar-left">
          <ul class="dropdown dropdownnav menu menunav" data-dropdown-menu>
            <a href="index.php"><li class="menu-text menu-text-nav"><img src="contenu/img/california.png" class="imagenav"></li></a>
          </ul>
        </div>
        <div class="menu-centered">
          <ul class="menu menunav" data-responsive-menu="drilldown medium-dropdown">
            <li class="has-submenu">
              <a href="cv.php">Curriculum Vitae</a>
              <ul class="submenu menu menunav vertical" data-submenu>
                <li><a href="annexe.php">Annexes</a></li>
              </ul>
            </li>
            <li><a href="blog.php">Blog</a></li>
            <li><a href="euro.php">Euro 2016</a></li>
            <li><a href="formulaire.php">Formulaire</a></li>
            <li><a href="template.php">Template</a></li>
          </ul>
        </div>
      </div>

      <div class="orbit baniere" role="region" aria-label="Favorite Space Pictures" data-orbit>
        <ul class="orbit-container">
          <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous Slide</span>&#9664;</button>
          <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next Slide</span>&#9654;</button>
          <li class="orbit-slide is-active">
            <img src="contenu/img/banner1.png">
          </li>
          <li class="orbit-slide">
            <img src="contenu/img/banner2.png">
          </li>
          <li class="orbit-slide">
            <img src="contenu/img/banner3.png">
          </li>
        </ul>
      </div>
      <!-- Header -->
      <header class="row site-header">
      </header>
      <!-- Section -->
        <section class="row site-content">
        </section>
        <!-- Footer -->
        <footer class="site-footer">
          <div class="row">
            <div class="small-12 medium-12 large-4 columns footertop footertopright">
              <p class="footer-links">
                <a href="cv.php">Curriculum Vitae</a>
                <a href="blog.php">Blog</a>
                <a href="euro.php">Euro 2016</a>
                <a href="formulaire.php">Formulaire</a>
              </p>
              <p class="footer-copywrite">Copywrite Loïc Dufresne © 2016</p>
            </div>
            <div class="small-12 medium-6 large-4 columns footertop footertopcenter">
              <ul class="contact">
                <li><p><i class="fi-male"></i>Loïc Dufresne</p></li>
                <li><p><i class="fi-marker"></i>1564 Domdidier</p></li>
                <li><p><i class="fi-telephone"></i>079 519 45 27</p></li>
                <li><p><a href="formulaire.php"><i class="fi-mail"></i>loic.dufresne@bluewin.ch</a></p></li>
              </ul>
            </div>
            <div class="small-12 medium-6 large-4 columns footertop footertopleft">
              <p class="footer-text">Follow me on :</p><br>
              <a href="https://www.facebook.com/dufresne.loic" target="_blank" class="first-links fi-social-facebook"></a>
              <a href="https://twitter.com/3DtoCOMPTON" target="_blank" class="first-links fi-social-twitter"></a>
              <a href="https://plus.google.com/100604848590200407608/posts" target="_blank" class="first-links fi-social-google-plus"></a><br><br>
              <a href="https://www.instagram.com/iam_dudu/" target="_blank" class="first-links fi-social-instagram"></a>
              <a href="https://www.youtube.com/channel/UC--YfpWVZ2Y5ais0XjZkQlg" target="_blank" class="first-links fi-social-youtube"></a>
              <a href="https://www.spotify.com/ch-fr/" target="_blank" class="first-links fi-social-spotify"></a>
            </div>
              <a href="blog.php">
                <div class="small-12 medium-12 large-12 columns footercenterleft">
                  <p class="second-links">Jetez un coup d'oeil aux derniers articles !</p>
                </div>
              </a>
              <a href="annexe.php">
                <div class="small-12 medium-12 large-12 columns footercentercenter">
                  <p class="second-links test">Découvrez mes projets ici !</p>
                </div>
              </a>
              <a href="formulaire.php">
                <div class="small-12 medium-12 large-12 columns footercenterright">
                  <p class="second-links">Contactez moi !</p>
                </div>
              </a>
          </div>
        </footer>
        <div id="map">
        </div>
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJnR5M5g9XBX64Bn0aKeYer0O2GIBEdcY&callback=initMap">
    </script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>

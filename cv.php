<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dufresne Loïc | Curriculum Vitae</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/buttons.css" />
    <link rel="stylesheet" href="css/stick.css">
    <link  href="http://fonts.googleapis.com/css?family=Reenie+Beanie:regular" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="foundation-icons/foundation-icons.css">
  </head>
  <body>
      <div class="title-bar title-bar-nav" data-responsive-toggle="main-menu" data-hide-for="medium">
        <a href="index.php"><div class="title-bar-title"><img src="contenu/img/california.png" class="imagenavsmall"></div></a>
        <button class="menu-icon" type="button" data-toggle></button>
        <div class="title-bar-title">Menu</div>
      </div>
      <div class="top-bar top-bar-nav" id="main-menu">
        <div class="top-bar-left">
          <ul class="dropdown dropdownnav menu menunav" data-dropdown-menu>
            <a href="index.php"><li class="menu-text menu-text-nav"><img src="contenu/img/california.png" class="imagenav"></li></a>
          </ul>
        </div>
        <div class="menu-centered">
          <ul class="menu menunav" data-responsive-menu="drilldown medium-dropdown">
            <li class="has-submenu">
              <a href="cv.php">Curriculum Vitae</a>
              <ul class="submenu menu menunav vertical" data-submenu>
                <li><a href="annexe.php">Annexes</a></li>
              </ul>
            </li>
            <li><a href="blog.php">Blog</a></li>
            <li><a href="euro.php">Euro 2016</a></li>
            <li><a href="formulaire.php">Formulaire</a></li>
          </ul>
        </div>
      </div>

      <div class="orbit baniere" role="region" aria-label="Favorite Space Pictures" data-orbit>
        <ul class="orbit-container">
          <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous Slide</span>&#9664;</button>
          <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next Slide</span>&#9654;</button>
          <li class="orbit-slide is-active">
            <img src="contenu/img/banner1.png">
          </li>
          <li class="orbit-slide">
            <img src="contenu/img/banner2.png">
          </li>
          <li class="orbit-slide">
            <img src="contenu/img/banner3.png">
          </li>
        </ul>
      </div>
      <!-- Header -->
      <header class="row site-header">
      </header>
      <!-- Section -->
        <section class="row site-content">
          <div class="columns index_contents">
            <h1 class="indexTitre">Curriculum Vitae</h1>
            <div class="columns small-12 medium-12 large-2 index_imageCV1">
              <img src="contenu/img/cv.png" class="index_imageCV">
            </div>
            <div class="columns small-12 medium-12 large-10 index_texteCV">
              <table>
                <tr>
                  <td class="index_nom"><strong>Loïc Dufresne</strong></td>
                  <td class="index_nom"><strong>Informaticien</strong></td>
                  <td class="index_nom"><strong></strong></td>
                  <td class="index_nom"><strong></strong></td>
                </tr>
                <tr>
                  <td><strong>Date de naissance :</strong></td>
                  <td>23 juin 1991</td>
                  <td><strong>Etat civil :</strong></td>
                  <td>Célibataire</td>
                </tr>
                <tr>
                  <td><strong>Origine :</strong></td>
                  <td>1860 Aigle VD</td>
                  <td><strong>Domicile :</strong></td>
                  <td>1564 Domdidier</td>
                </tr>
                <tr>
                  <td><strong>Rue :</strong></td>
                  <td>Grand-Rhain 41</td>
                  <td><strong>E-mail :</strong></td>
                  <td>
                    <a class="index_popup" href="mailto:loic.dufresne@bluewin.ch">loic.dufresne@bluewin.ch
                      <span>Envoyer un mail...</span>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td><strong>Téléphone fixe :</strong></td>
                  <td>026/675.27.45</td>
                  <td><strong>Téléphone portable :</strong></td>
                  <td>079/519.45.27</td>
                </tr>
              </table>

            </div>
            <div class="columns small-12 medium-12 large-12">
              <div><hr></div>
              <h2>Etudes</h2>
              <table>
                <tr>
                  <td class="index_ligne"><strong>2015 - … :</strong></td>
                  <td>Haute école d'ingénierie et d'architecture de Fribourg</td>
                </tr>
                <tr>
                  <td></td>
                  <td>Formation de télécommunication, filière internet et communication</td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <a class="index_popup" href="https://www.heia-fr.ch" target="_blank">Homepage | Haute école d'ingénierie et d'architecture de Fribourg
                      <span>Accéder au site...</span>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td><strong>2008 - 2014 :</strong></td>
                  <td>Apprentissage d’informaticien avec maturité professionnelle</td>
                </tr>
                <tr>
                  <td></td>
                  <td>Ecole des Métiers | technique et art | Fribourg</td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <a class="index_popup" href="http://www.emf.ch" target="_blank">Homepage | Ecole des Métiers | technique et art | Fribourg
                      <span>Accéder au site...</span>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td><strong>2003 - 2008 :</strong></td>
                  <td>Cycle d’orientation de la Broye, Domdidier</td>
                </tr>
                <tr>
                  <td><strong>1997 - 2003 :</strong></td>
                  <td>Ecole primaire, Domdidier</td>
                </tr>
              </table>
              <div><hr></div>
              <h2>Stages</h2>
              <table>
                <tr>
                  <td class="index_ligne"><strong>2013 :</strong></td>
                  <td>Stage de 4 mois, domaine du développement</td>
                </tr>
                <tr>
                  <td></td>
                  <td>CISOFT, Estavayer-le-Lac FR</td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <a class="index_popup" href="http://www.cisoft.ch" target="_blank">Homepage | CISOFT
                      <span>Accéder au site...</span>
                    </a>
                  </td>
                </tr>
              </table>
              <div><hr></div>
              <h2>Certificats, diplomes & distinctions</h2>
              <table>
                <tr>
                  <td class="index_ligne"><strong>2014 :</strong></td>
                  <td>Certificat de maturité professionnelle</td>
                </tr>
                <tr>
                  <td></td>
                  <td>Certificat fédéral de capacité - Informaticien</td>
                </tr>
                <tr>
                  <td></td>
                  <td>Distinction du meilleur travail pratique individuel d’examen final d’informaticien de Suisse – ICT Young Professional Award 2014</td>
                </tr>
                <tr>
                  <td><strong>2013 :</strong></td>
                  <td>Certificat de stage de l’entreprise CISOFT</td>
                </tr>
                <tr>
                  <td><strong>2008 :</strong></td>
                  <td>Certificat de fin d’études obligatoire, section pré-gymnasiale</td>
                </tr>
              </table>
              <div class="bg-1">
                <a href="annexe.php">
                <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Télécharger annexes</span></button>
                </a>
              </div>
              <div><hr></div>
              <h2>Connaissances des langues</h2>
              <table>
                <tr>
                  <td><strong>Français :</strong></td>
                  <td>Langue maternelle</td>
                </tr>
                <tr>
                  <td><strong>Allemand :</strong></td>
                  <td>Maturité professionnelle, niveau B2 selon le Portfolio européen des langues</td>
                </tr>
                <tr>
                  <td><strong>Anglais :</strong></td>
                  <td>Maturité professionnelle, niveau B2 selon le Portfolio européen des langues</td>
                </tr>
              </table>
              <div><hr></div>
              <h2>Expériences professionnelles</h2>
              <table>
                <tr>
                  <td class="index_ligne"><strong>2014 - 2015 :</strong></td>
                  <td>Service militaire</td>
                </tr>
                <tr>
                  <td></td>
                  <td>Ecole UEM/FU S 61, aide au commandement, Frauenfeld TG</td>
                </tr>
                <tr>
                  <td></td>
                  <td>Pionnier informatique</td>
                </tr>
                <tr>
                  <td><strong>2012 - 2016 :</strong></td>
                  <td>EMS Les Grèves du Lac, Gletterens FR</td>
                </tr>
                <tr>
                  <td></td>
                  <td>Entretien et travaux du bâtiment, aide-jardinier</td>
                </tr>
                <tr>
                  <td></td>
                  <td>Maintenance et dépannage informatique</td>
                </tr>
              </table>
              <div><hr></div>
              <h2>Loisirs</h2>
              <table>
                <tr>
                  <td class="index_ligne"><strong>Sports :</strong></td>
                  <td>Football, FC Domdidier 4ème ligue, Domdidier</td>
                </tr>
                <tr>
                  <td></td>
                  <td>Ski Club, Domdidier</td>
                </tr>
                <tr>
                  <td><strong>Création :</strong></td>
                  <td>Adobe InDesign, Illustrator & Photoshop</td>
                </tr>
                <tr>
                  <td><strong>Multimédias :</strong></td>
                  <td>Vidéo, photo, musique</td>
                </tr>
                <tr>
                  <td><strong>Autres :</strong></td>
                  <td>Dessin, géographie, histoire, randonnée</td>
                </tr>
              </table>
            </div>
            <div class="columns small-12 medium-12 large-12 bg-1">
              <a href="contenu/documents/F_CV_dufresne_loic.pdf" target="_blank">
              <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Télécharger CV</span></button>
              </a>
              <a href="annexe.php">
              <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Télécharger annexes</span></button>
              </a>
            </div>

            <div class="columns small-12 medium-12 large-12">
              <ul class="stick_ul">
                <li class="stick_li">
                  <a href="contenu/documents/Bulletin1.pdf" target="_blank" class="stick_a">
                    <h2 class="stick_h2">2016</h2>
                    <p class="stick_p">HEIAFR - Bulletin de notes semestre 1</p>
                  </a>
                </li>
                <li class="stick_li">
                  <a href="contenu/documents/F_CV_dufresne_loic.pdf" target="_blank" class="stick_a">
                    <h2 class="stick_h2">2016</h2>
                    <p class="stick_p">Curriculum Vitae version Suisse</p>
                  </a>
                </li>
                <li class="stick_li">
                  <a href="contenu/documents/USA_CV_dufresne_loic.pdf" target="_blank" class="stick_a">
                    <h2 class="stick_h2">2016</h2>
                    <p class="stick_p">Curriculum Vitae version Américaine</p>
                  </a>
                </li>
                <li class="stick_li">
                  <a href="contenu/documents/batMine.jar" class="stick_a">
                    <h2 class="stick_h2">2016</h2>
                    <p class="stick_p">Projet: BatMine</p>
                  </a>
                </li>
                <li class="stick_li">
                  <a href="contenu/documents/Pastis51.jar" class="stick_a">
                    <h2 class="stick_h2">2016</h2>
                    <p class="stick_p">Projet: Pastis</p>
                  </a>
                </li>
                <li class="stick_li">
                  <a href="#" class="stick_a">
                    <h2 class="stick_h2">2014</h2>
                    <p class="stick_p">Certificat de maturité professionnell</p>
                  </a>
                </li>
                <li class="stick_li">
                  <a href="#" class="stick_a">
                    <h2 class="stick_h2">2014</h2>
                    <p class="stick_p">Certificat fédéral de capacité - Informaticien</p>
                  </a>
                </li>
                <li class="stick_li">
                  <a href="#" class="stick_a">
                    <h2 class="stick_h2">2014</h2>
                    <p class="stick_p">ICT Young Professional Award 2014</p>
                  </a>
                </li>
                <li class="stick_li">
                  <a href="contenu/documents/DUFRESNE_NAOVoiceDriven_Rapport.pdf" target="_blank" class="stick_a">
                    <h2 class="stick_h2">2014</h2>
                    <p class="stick_p">NAO Voice Driven - Travail professionnel individuel</p>
                  </a>
                </li>
                <li class="stick_li">
                  <a href="#" class="stick_a">
                    <h2 class="stick_h2">2013</h2>
                    <p class="stick_p">Certificat de stage de l’entreprise CISOFT</p>
                  </a>
                </li>
                <li class="stick_li">
                  <a href="#" class="stick_a">
                    <h2 class="stick_h2">2008</h2>
                    <p class="stick_p">Certificat de fin d’études obligatoire, section pré-gymnasiale</p>
                  </a>
                </li>
              </ul>
            </div>

          </div>
        </section>
        <!-- Footer -->
        <footer class="site-footer">
          <div class="row">
            <div class="small-12 medium-12 large-4 columns footertop footertopright">
              <p class="footer-links">
                <a href="cv.php">Curriculum Vitae</a>
                <a href="blog.php">Blog</a>
                <a href="euro.php">Euro 2016</a>
                <a href="formulaire.php">Formulaire</a>
              </p>
              <p class="footer-copywrite">Copywrite Loïc Dufresne © 2016</p>
            </div>
            <div class="small-12 medium-6 large-4 columns footertop footertopcenter">
              <ul class="contact">
                <li><p><i class="fi-male"></i>Loïc Dufresne</p></li>
                <li><p><i class="fi-marker"></i>1564 Domdidier</p></li>
                <li><p><i class="fi-telephone"></i>079 519 45 27</p></li>
                <li><p><a href="formulaire.php"><i class="fi-mail"></i>loic.dufresne@bluewin.ch</a></p></li>
              </ul>
            </div>
            <div class="small-12 medium-6 large-4 columns footertop footertopleft">
              <p class="footer-text">Follow me on :</p><br>
              <a href="https://www.facebook.com/dufresne.loic" target="_blank" class="first-links fi-social-facebook"></a>
              <a href="https://twitter.com/3DtoCOMPTON" target="_blank" class="first-links fi-social-twitter"></a>
              <a href="https://plus.google.com/100604848590200407608/posts" target="_blank" class="first-links fi-social-google-plus"></a><br><br>
              <a href="https://www.instagram.com/iam_dudu/" target="_blank" class="first-links fi-social-instagram"></a>
              <a href="https://www.youtube.com/channel/UC--YfpWVZ2Y5ais0XjZkQlg" target="_blank" class="first-links fi-social-youtube"></a>
              <a href="https://www.spotify.com/ch-fr/" target="_blank" class="first-links fi-social-spotify"></a>
            </div>
              <a href="blog.php">
                <div class="small-12 medium-12 large-12 columns footercenterleft">
                  <p class="second-links">Jetez un coup d'oeil aux derniers articles !</p>
                </div>
              </a>
              <a href="annexe.php">
                <div class="small-12 medium-12 large-12 columns footercentercenter">
                  <p class="second-links test">Découvrez mes projets ici !</p>
                </div>
              </a>
              <a href="formulaire.php">
                <div class="small-12 medium-12 large-12 columns footercenterright">
                  <p class="second-links">Contactez moi !</p>
                </div>
              </a>
          </div>
        </footer>
        <div id="map">
        </div>
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJnR5M5g9XBX64Bn0aKeYer0O2GIBEdcY&callback=initMap">
    </script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>

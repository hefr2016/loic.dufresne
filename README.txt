1. Cloner le projet:

```
#!php

git clone https://hefr2016@bitbucket.org/hefr2016/loic.dufresne.git YOUR-PROJECT-NAME

```

2. Créer la base de données dufresne_blog

3. Importer les données du fichier dufresne_blog.sql dans la base de données
-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Mar 28 Juin 2016 à 12:08
-- Version du serveur :  5.5.42
-- Version de PHP :  7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dufresne_blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `auteur_id` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `ImgURL` varchar(255) NOT NULL,
  `coordonnees` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`id`, `date`, `auteur_id`, `titre`, `body`, `ImgURL`, `coordonnees`) VALUES
(1, '2016-06-03', 1, 'Les chutes d''Havasu', 'Les chutes d''Havasu (en anglais : Havasu Falls et en Havasupai : Havasuw Hagjahgeevma) est une chute d''eau d''Havasu Creek située dans le Grand Canyon en Arizona, États-Unis.\r\nLa forte concentration de carbonate de calcium donne une couleur bleue-verte à l''eau.', 'contenu/article/havasu/titre.png', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3167.090289962792!2d-112.69997184919599!3d36.25499997996675!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzbCsDE1JzE4LjAiTiAxMTLCsDQxJzUyLjAiVw!5e1!3m2!1sfr!2sfr!4v1465153227341'),
(2, '2016-06-01', 1, 'Le Grand Canyon', 'Le Grand Canyon, en anglais Grand Canyon, en hopi Ongtupqa, est un canyon des États-Unis situé dans le Nord-Ouest de l''Arizona. La gorge a été creusée par le fleuve Colorado dans le plateau du même nom. Fondé en 1919, le Parc National du Grand Canyon couvre un territoire protégé de 4''927 km2. Il est fréquenté chaque année par plus de quatre millions de visiteurs. La diversité et la singularité naturelle et paysagère du site l''ont fait reconnaître patrimoine mondial de l’Humanité par l’UNESCO en 1979.<br><br>\r\nLe Grand Canyon est situé dans le sud-ouest des États-Unis, dans le nord de l''État de l''Arizona.\r\n<br><br>\r\nLes dimensions du Grand Canyon sont gigantesques, d''où son nom (en anglais, grand ne signifie pas grand, mais à la fois immense, imposant, grandiose et superbe). Il s''étend sur environ 450 km de long1 entre le lac Powell et le lac Mead, dont 350 km sont situés dans le parc national. Sa profondeur moyenne est de 1''300 mètres, avec un maximum de plus de 2''000 mètres. Sa largeur varie de 5,5 km à 30 km3.', 'contenu/article/grandCanyon/titre.png', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d58068.52464832557!2d-112.1705369531233!3d36.081527622003755!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzbCsDAzJzI5LjUiTiAxMTLCsDA4JzI2LjEiVw!5e1!3m2!1sfr!2sfr!4v1465154643007'),
(3, '2016-06-10', 1, 'Los Angeles', 'Los Angeles est la deuxième ville des États-Unis après New York. Elle est située dans le sud de l''État de Californie, sur la côte pacifique. Les Américains l''appellent souvent par son diminutif, L.A.. Cette ville est le siège du comté portant le même nom. Los Angeles signifie « les anges » en espagnol et ses habitants sont appelés Angelenos (quelquefois Angelinos) – les Angelins. Son nom complet est El Pueblo de Nuestra Señora la Reina de Los Ángeles del Río de Porciúncula, c''est-à-dire « le village de Notre-Dame la Reine des Anges du fleuve de Porciuncula », nom donné au cours d''eau par les Amérindiens.<br><br>\r\nSelon le bureau du recensement des États-Unis, la population de la commune est de 3''884''307 habitants en 2013, alors qu''elle n''était que de 11''500 en 1887. Le comté rassemble 10''179''716 habitants (en juillet 2004) alors que l''aire urbaine de Los Angeles compte environ 18,5 millions d’habitants, ce qui en fait la deuxième agglomération des États-Unis après celle de New York. Mais la commune de Los Angeles est relativement restreinte face à son agglomération, même si elle est plus grande que New York et Chicago. Los Angeles est une ville olympique : elle a accueilli les Jeux deux fois (en 1932 et 1984). Mondialement connue pour son activité culturelle (Hollywood), elle a un statut de ville-région mondiale (world city-region)5. Ville cosmopolite, elle demeure l''un des points d''entrée d''immigrants les plus importants aux États-Unis.<br>\r\n<h2>Géographie</h2><br>\r\nSituée en Californie du Sud, bordée au nord par les Monts San Gabriel, à l''ouest et au sud par l''Océan Pacifique (situation favorable permettant aux Angelins de faire du ski et se baigner dans la même journée), Los Angeles offre une grande variété de paysages. Le rivage est constitué des longues plages de sable blanc des baies de Santa Monica et de San Pedro, qui font de Los Angeles la plus grande métropole établie sur un site de littoral, Perth et Rio de Janeiro exceptées9. La ville occupe une partie du bassin de Los Angeles, une plaine côtière accidentée, et une grande partie de la vallée de San Fernando, dont elle est séparée par de hautes collines, les monts Santa Monica. Le principal cours d''eau de Los Angeles est la Los Angeles River, un petit fleuve qui prend sa source dans la vallée de San Fernando et traverse la ville jusqu''à l''océan.<br><br>\r\nLa ville s''étend sur 1 290,6 km2 (dont 75,7 km2, soit 5,86 %, de plans d''eau). La distance nord-sud la plus grande est de 71 km, la distance est-ouest de 47 km ; le périmètre total est de 550 km. Selon le Bureau du recensement des États-Unis, Los Angeles est à la septième place nationale des villes de plus de 100''000 habitants pour la superficie. L''altitude maximale sur la commune est de 1''548 m au Sister Elsie Peak.<br><br>\r\nLa région de Los Angeles comprend un nombre remarquable d''espèces de plantes indigènes : le pavot de Californie, le matilija poppy, le toyon, et des centaines d''autres. Avec ses plages, ses dunes, ses collines, ses montagnes et ses rivières, elle est riche en écosystèmes divers. Mais certaines espèces sont rares et menacées, comme la Los Angeles sunflower. La ville compte également 379 parcs dont la superficie totale est de 63,5 km2. Griffith Park est le plus grand parc urbain du monde. Le plus vieux parc de la ville a été créé en 1781 et se trouve dans le El Pueblo de Los Angeles Historic Monument, près de Union Station.\r\n', 'contenu/article/losAngeles/titre.png', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d833891.1815745402!2d-118.1636973854003!3d33.96447500873208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzTCsDAzJzAwLjAiTiAxMTjCsDE1JzAwLjAiVw!5e1!3m2!1sfr!2sch!4v1465155468995');

-- --------------------------------------------------------

--
-- Structure de la table `article_tag`
--

CREATE TABLE `article_tag` (
  `tag_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `article_tag`
--

INSERT INTO `article_tag` (`tag_id`, `article_id`) VALUES
(2, 1),
(3, 1),
(5, 1),
(6, 1),
(7, 1),
(2, 2),
(3, 2),
(5, 2),
(6, 2),
(1, 3),
(4, 3),
(5, 3);

-- --------------------------------------------------------

--
-- Structure de la table `auteur`
--

CREATE TABLE `auteur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `pseudo` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `auteur`
--

INSERT INTO `auteur` (`id`, `nom`, `prenom`, `pseudo`) VALUES
(1, 'Dufresne', 'Loïc', 'Dudu');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `imggallery` varchar(255) NOT NULL,
  `article_id` int(11) NOT NULL,
  `legende` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`id`, `imggallery`, `article_id`, `legende`) VALUES
(1, 'contenu/article/havasu/images/havasu1.png', 1, 'Chutes d''Havasu'),
(2, 'contenu/article/havasu/images/havasu2.png', 1, 'Chutes d''Havasu'),
(6, 'contenu/article/grandCanyon/images/canyon1.png', 2, 'Grand Canyon'),
(7, 'contenu/article/havasu/images/havasu4.png', 1, 'Chutes d''Havasu'),
(8, 'contenu/article/havasu/images/havasu3.png', 1, 'Chutes d''Havasu'),
(9, 'contenu/article/havasu/images/havasu6.png', 1, 'Chutes d''Havasu'),
(10, 'contenu/article/havasu/images/havasu5.png', 1, 'Chutes d''Havasu'),
(11, 'contenu/article/grandCanyon/images/canyon2.png', 2, 'Grand Canyon'),
(12, 'contenu/article/grandCanyon/images/canyon3.png', 2, 'Grand Canyon'),
(13, 'contenu/article/grandCanyon/images/canyon4.png', 2, 'Grand Canyon'),
(14, 'contenu/article/losAngeles/images/la1.png', 3, 'Los Angeles Downtown'),
(15, 'contenu/article/losAngeles/images/la2.png', 3, 'Los Angeles Downtown'),
(16, 'contenu/article/losAngeles/images/la3.png', 3, 'Los Angeles Interstate'),
(17, 'contenu/article/losAngeles/images/la5.png', 3, 'Hollywood'),
(18, 'contenu/article/losAngeles/images/la6.png', 3, 'Hollywood'),
(19, 'contenu/article/losAngeles/images/la4.png', 3, 'Beverly Hills'),
(20, 'contenu/article/losAngeles/images/la7.png', 3, 'Malibu Beach'),
(21, 'contenu/article/losAngeles/images/la8.png', 3, 'Los Angeles Hills'),
(22, 'contenu/article/losAngeles/images/la9.png', 3, 'Universal Studio Hollywood'),
(23, 'contenu/article/losAngeles/images/la10.png', 3, 'Santa Monica');

-- --------------------------------------------------------

--
-- Structure de la table `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `tag`
--

INSERT INTO `tag` (`id`, `name`) VALUES
(1, 'Californie'),
(2, 'Canyon'),
(3, 'Arizona'),
(4, 'Los Angeles'),
(5, 'USA'),
(6, 'Colorado'),
(7, 'Chutes');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `auteur`
--
ALTER TABLE `auteur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `auteur`
--
ALTER TABLE `auteur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

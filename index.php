<?php
  //Connexion a la base de données
  $db = new PDO('mysql:host=localhost;dbname=dufresne_blog;charset=utf8mb4', 'root', 'root');

$query = $db->prepare(" SELECT article.id articleid, article.titre, article.date, article.body, article.imgURL, article.auteur_id, auteur.id auteurid, auteur.nom
                        FROM article
                        INNER JOIN auteur ON auteur.id=article.auteur_id
                        LIMIT 0,3");
$query->execute();
$links = $query->fetchAll();
?>
<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dufresne Loïc | Index</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/buttons.css" />
    <link rel="stylesheet" href="css/stick.css">
    <link  href="http://fonts.googleapis.com/css?family=Reenie+Beanie:regular" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="foundation-icons/foundation-icons.css">
  </head>
  <body>
    <a href="cv.php">
      <div class="index_index" style="background-image: url('contenu/img/backgroundBody.png')">
      <div class="index_indexCover" data-color="black"></div>
        <div>
          <h1 class="index_indexTitre">
            Loïc Dufresne
          </h1>
          <div>
            <h4 class="index_indexSousTitre">Curriculum Vitae | Trips | Pictures</h4>
    			</div>
          <img src="contenu/img/california.png" class="index_indexImage">
        </div>
        <span href="#ancre" class="lnr lnr-chevron-down"></span>
      </div>
    </a>
    <div id="ancre">
      <?php foreach ($links as $link):?>
        <a href="article.php?id=<?php echo $link['articleid']?>">
        <div class="columns small-12 medium-6 large-4 article_links">
          <div class="columns small-12 medium-12 large-12 article_linksContents">
            <p class="article_linksTitre"><?php echo $link['titre'];?></p>
            <div class="columns small-4 medium-4 large-4">
              <img src="<?php echo $link['imgURL'];?>" class="article_linksImage">
            </div>
            <div class="columns small-8 medium-8 large-8 article_linksText">
              <?php echo substr ($link['body'],0,110)?>...
              <p class="blog_suiteArticle">Lire la suite...</p>
            </div>
            <div class="columns small-12 medium-12 large-12 article_linksAuteur">
              <?php echo $link['date'] ;?>
              <?php echo $link['nom'] ;?>
            </div>
          </div>
        </div>
      </a>
    <?php endForeach; ?>
  </div>
  </body>
</html>

<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dufresne Loïc | Annexe</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/buttons.css">
    <link rel="stylesheet" href="foundation-icons/foundation-icons.css">
    </head>
    <body>
        <div class="title-bar title-bar-nav" data-responsive-toggle="main-menu" data-hide-for="medium">
          <a href="index.php"><div class="title-bar-title"><img src="contenu/img/california.png" class="imagenavsmall"></div></a>
          <button class="menu-icon" type="button" data-toggle></button>
          <div class="title-bar-title">Menu</div>
        </div>
        <div class="top-bar top-bar-nav" id="main-menu">
          <div class="top-bar-left">
            <ul class="dropdown dropdownnav menu menunav" data-dropdown-menu>
              <a href="index.php"><li class="menu-text menu-text-nav"><img src="contenu/img/california.png" class="imagenav"></li></a>
            </ul>
          </div>
          <div class="menu-centered">
            <ul class="menu menunav" data-responsive-menu="drilldown medium-dropdown">
              <li class="has-submenu">
                <a href="cv.php">Curriculum Vitae</a>
                <ul class="submenu menunav menu vertical" data-submenu>
                  <li><a href="annexe.php">Annexes</a></li>
                </ul>
              </li>
              <li><a href="blog.php">Blog</a></li>
              <li><a href="euro.php">Euro 2016</a></li>
              <li><a href="formulaire.php">Formulaire</a></li>
            </ul>
          </div>
        </div>

        <div class="orbit baniere" role="region" aria-label="Favorite Space Pictures" data-orbit>
          <ul class="orbit-container">
            <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous Slide</span>&#9664;</button>
            <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next Slide</span>&#9654;</button>
            <li class="orbit-slide is-active">
              <img src="contenu/img/banner1.png">
            </li>
            <li class="orbit-slide">
              <img src="contenu/img/banner2.png">
            </li>
            <li class="orbit-slide">
              <img src="contenu/img/banner3.png">
            </li>
          </ul>
        </div>
        <!-- Header -->
        <header class="row site-header">
        </header>
        <!-- Section -->
        <section class="row site-content">
          <div class="columns index_contents annexe_contents">
            <h1>Annexes</h1>
            <h3>CV | Diplômes | Certificats | Projets</h3>
            <h4>2016</h4>
            <button class="accordion">Afficher</button>
            <div class="panel">
              <p><strong>Bulletin de notes semestre 1 - Haute école d'ingénierie et d'architecture de Fribourg :</strong></p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/Bulletin1.pdf" target="_blank">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Ouvrir & Télécharger</span></button>
                </a>
              </div>
              <div><hr></div>
              <p><strong>Curriculum Vitae version Suisse :</strong></p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/F_CV_dufresne_loic.pdf" target="_blank">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Ouvrir & Télécharger</span></button>
                </a>
              </div>
              <div><hr></div>
              <p><strong>Curriculum Vitae version Américaine :</strong></p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/USA_CV_dufresne_loic.pdf" target="_blank">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Ouvrir & Télécharger</span></button>
                </a>
              </div>
              <div><hr></div>
              <p><strong>Projet :</strong></p>
              <p>BatMine: Application du jeu du Démineur version Batman réalisée dans le cadre du cours Interface-Homme-Machine à l'Ecole d'ingénierie de Frbourg, développée en Java</p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/batMine.jar">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Télécharger</span></button>
                </a>
              </div>
              <p>Pastis: Application génératrice de mots de passe réalisée dans le cadre du cours Interface-Homme-Machine à l'Ecole d'ingénierie de Frbourg, développée en Java</p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/Pastis51.jar">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Télécharger</span></button>
                </a>
              </div>
              <p>BattleShip: Application du jeu BattleShip revisitée par nos soinsdans le cadre du cours Interface-Homme-Machine à l'Ecole d'ingénierie de Frbourg, développée en Java</p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/BattleShip.jar">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Télécharger</span></button>
                </a>
              </div>
            </div>
            <div><hr></div>
            <h4>2014</h4>
            <button class="accordion">Afficher</button>
            <div class="panel">
              <p><strong>Certificat de maturité professionnelle :</strong></p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/fichier/#" target="_blank">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Ouvrir & Télécharger</span></button>
                </a>
              </div>
              <div><hr></div>
              <p><strong>Certificat fédéral de capacité - Informaticien :</strong></p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/fichier/#" target="_blank">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Ouvrir & Télécharger</span></button>
                </a>
              </div>
              <div><hr></div>
              <p><strong>Distinction du meilleur travail pratique individuel d’examen final d’informaticien de Suisse – ICT Young Professional Award 2014 :</strong></p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/fichier/#" target="_blank">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Ouvrir & Télécharger</span></button>
                </a>
              </div>
              <div><hr></div>
              <p><strong>NAO Voice Driven - Travail professionnel individuel - Informaticien CFC :</strong></p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/DUFRESNE_NAOVoiceDriven_Rapport.pdf" target="_blank">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Ouvrir & Télécharger</span></button>
                </a>
              </div>
            </div>
            <div><hr></div>
            <h4>2013</h4>
            <button class="accordion">Afficher</button>
            <div class="panel">
              <p><strong>Certificat de stage de l’entreprise CISOFT :</strong></p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/fichier/#" target="_blank">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Ouvrir & Télécharger</span></button>
                </a>
              </div>
            </div>
            <div><hr></div>
            <h4>2008</h4>
            <button class="accordion">Afficher</button>
              <div class="panel">
              <p><strong>Certificat de fin d’études obligatoire, section pré-gymnasiale :</strong></p>
              <div class="columns small-12 medium-12 large-12 bg-1">
                <a href="contenu/documents/fichier/#" target="_blank">
                  <button class="button button--naira button--round-s button--border-thin"><i class="button__icon icon icon-download"></i><span>Ouvrir & Télécharger</span></button>
                </a>
              </div>
            </div>
            <div><hr></div>
            <script>
              var acc = document.getElementsByClassName("accordion");
              var i;
              for (i = 0; i < acc.length; i++) {
                  acc[i].onclick = function(){
                      this.classList.toggle("active");
                      this.nextElementSibling.classList.toggle("show");
                }
              }
            </script>
          </div>
        </section>
        <!-- Footer -->
        <footer class="site-footer">
          <div class="row">
            <div class="small-12 medium-12 large-4 columns footertop footertopright">
              <p class="footer-links">
                <a href="cv.php">Curriculum Vitae</a>
                <a href="blog.php">Blog</a>
                <a href="euro.php">Euro 2016</a>
                <a href="formulaire.php">Formulaire</a>
              </p>
              <p class="footer-copywrite">Copywrite Loïc Dufresne © 2016</p>
            </div>
            <div class="small-12 medium-6 large-4 columns footertop footertopcenter">
              <ul class="contact">
                <li><p><i class="fi-male"></i>Loïc Dufresne</p></li>
                <li><p><i class="fi-marker"></i>1564 Domdidier</p></li>
                <li><p><i class="fi-telephone"></i>079 519 45 27</p></li>
                <li><p><a href="formulaire.php"><i class="fi-mail"></i>loic.dufresne@bluewin.ch</a></p></li>
              </ul>
            </div>
            <div class="small-12 medium-6 large-4 columns footertop footertopleft">
              <p class="footer-text">Follow me on :</p><br>
              <a href="https://www.facebook.com/dufresne.loic" target="_blank" class="first-links fi-social-facebook"></a>
              <a href="https://twitter.com/3DtoCOMPTON" target="_blank" class="first-links fi-social-twitter"></a>
              <a href="https://plus.google.com/100604848590200407608/posts" target="_blank" class="first-links fi-social-google-plus"></a><br><br>
              <a href="https://www.instagram.com/iam_dudu/" target="_blank" class="first-links fi-social-instagram"></a>
              <a href="https://www.youtube.com/channel/UC--YfpWVZ2Y5ais0XjZkQlg" target="_blank" class="first-links fi-social-youtube"></a>
              <a href="https://www.spotify.com/ch-fr/" target="_blank" class="first-links fi-social-spotify"></a>
            </div>
              <a href="blog.php">
                <div class="small-12 medium-12 large-12 columns footercenterleft">
                  <p class="second-links">Jetez un coup d'oeil aux derniers articles !</p>
                </div>
              </a>
              <a href="annexe.php">
                <div class="small-12 medium-12 large-12 columns footercentercenter">
                  <p class="second-links test">Découvrez mes projets ici !</p>
                </div>
              </a>
              <a href="formulaire.php">
                <div class="small-12 medium-12 large-12 columns footercenterright">
                  <p class="second-links">Contactez moi !</p>
                </div>
              </a>
          </div>
        </footer>
        <div id="map">
        </div>
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJnR5M5g9XBX64Bn0aKeYer0O2GIBEdcY&callback=initMap">
    </script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
